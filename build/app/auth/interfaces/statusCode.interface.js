"use strict";
var emailStatusInit = (function () {
    function emailStatusInit() {
        this.emailStatus = {
            statusCode: 0,
            message: 'string',
            result: {
                message: 'string'
            },
            error: false
        };
    }
    return emailStatusInit;
}());
exports.emailStatusInit = emailStatusInit;
//# sourceMappingURL=statusCode.interface.js.map