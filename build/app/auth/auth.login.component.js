"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/** Core Services */
var core_1 = require('@angular/core');
//import { LocalStorageService } from 'angular-2-local-storage';
/** User defined Services */
var auth_service_1 = require('./auth.service');
/** Interfaces  */
var loginUser_interface_1 = require('./interfaces/loginUser.interface');
var loginComponent = (function () {
    function loginComponent(_authService) {
        this._authService = _authService;
        this.user = new loginUser_interface_1.userInit().user;
    }
    ;
    loginComponent.prototype.doLogin = function () {
        console.log(this.user);
        this._authService
            .loginUser(this.user)
            .subscribe(function (data) {
            //localStorage.setItem("userName", this.user.email)
        }, function (Error) { return console.log(Error); }, function () { return console.log("done"); });
    };
    loginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: "app/auth/login.html",
            providers: [auth_service_1.authService],
            styleUrls: []
        }), 
        __metadata('design:paramtypes', [auth_service_1.authService])
    ], loginComponent);
    return loginComponent;
}());
exports.loginComponent = loginComponent;
//# sourceMappingURL=auth.login.component.js.map