"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/** Core Services */
var core_1 = require('@angular/core');
var Rx_1 = require('rxjs/Rx');
/** User defined Services */
var auth_service_1 = require('./auth.service');
/** Interfaces  */
var user_interface_1 = require('./interfaces/user.interface');
var AuthComponent = (function () {
    function AuthComponent(_authService) {
        this._authService = _authService;
        this.checkEmail = new Rx_1.Subject(); // Creating new subject stream
        this.user = new user_interface_1.userInit().user; // Initilize User 
        this.emailStatus = {};
    }
    AuthComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.emailMessage = "Checking Email ..... ";
        this.checkEmail
            .distinctUntilChanged()
            .switchMap(function (input) { return _this._authService.validateEmail(input); })
            .subscribe(function (data) {
            _this.emailStatus = data;
            if (data['statusCode'] == 200 && data['message'] == "ok" && data['result']['message'] == 'verified') {
                _this.emailMessage = '';
            }
            else if (data['statusCode'] == 422 && data['message'] == 'email already registered') {
                _this.emailMessage = data['message'];
            }
            else if (data['statusCode'] == 422 && data['message'] == 'email should be registered with any email provider') {
                _this.emailMessage = data['message'];
            }
            else {
                _this.emailMessage = "Invaid Email ";
            }
        });
    };
    AuthComponent.prototype.registerUser = function () {
        var _this = this;
        this._authService
            .registerUser(this.user)
            .subscribe(function (data) { return console.log(JSON.stringify(data)); }, function (Error) { return console.log("There is an error"); }, function () { return _this.ngOnInit(); });
    };
    AuthComponent.prototype.validateEmail = function (email) {
        this.checkEmail
            .next(email);
    };
    AuthComponent = __decorate([
        core_1.Component({
            selector: 'Register',
            templateUrl: "app/auth/register.html",
            providers: [auth_service_1.authService],
            styleUrls: []
        }), 
        __metadata('design:paramtypes', [auth_service_1.authService])
    ], AuthComponent);
    return AuthComponent;
}());
exports.AuthComponent = AuthComponent;
//# sourceMappingURL=auth.register.component.js.map