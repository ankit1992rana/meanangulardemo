import { Component } from '@angular/core';

import {headerComponent} from '../shared/shared.header.component';

@Component({
    selector: 'landing',
    templateUrl: 'app/landing/landing.html',
    styleUrls : []
})

export class landingComponent {
     public title = "mindMax";
}
