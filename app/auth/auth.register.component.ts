/** Core Services */
import { Component, OnInit } from '@angular/core';
import {Observable, Subject} from 'rxjs/Rx';

/** User defined Services */
import { authService } from './auth.service'; 


/** Interfaces  */
import { userInit } from './interfaces/user.interface';

@Component({
    selector: 'Register',
    templateUrl: `app/auth/register.html`,
    providers : [authService],
    styleUrls : []
})

export class AuthComponent implements OnInit{
    private checkEmail = new Subject<string>(); // Creating new subject stream
    private user = new userInit().user; // Initilize User 
    private emailMessage:String;
    private emailStatus:Object;

    constructor(public _authService:authService){
        this.emailStatus = {}
    }

    ngOnInit():any{
        this.emailMessage = "Checking Email ..... ";
        this.checkEmail
            .distinctUntilChanged()
            .switchMap((input:string) => this._authService.validateEmail(input))
            .subscribe(
                data => { 
                    this.emailStatus = data
                 if (data['statusCode'] == 200 && data['message'] == "ok" && data['result']['message'] == 'verified') {
                   this.emailMessage = '';
                   
                } else if (data['statusCode'] == 422 && data['message'] == 'email already registered') {
                    this.emailMessage =  data['message'];
                   
                } else if (data['statusCode'] == 422 && data['message'] == 'email should be registered with any email provider') {
                    this.emailMessage =  data['message'];
                } else {
                    this.emailMessage = "Invaid Email ";
                }
            }
          )       
        }

    registerUser(){
        this._authService
        .registerUser(this.user)
        .subscribe(
            data => console.log(JSON.stringify(data)),
            Error => console.log("There is an error"),
            () => this.ngOnInit()        
        );
    }

    validateEmail(email:string){
        this.checkEmail
            .next(email);
    }
 }
